# Notes Backend API

## Containerd build

```bash
# Local execution
# OpenAPI Docs available via http://localhost:80/docs
uvicorn main:app --reload --port 80

# Build container
nerdctl build --tag note-api .

# Execute container
# OpenAPI Docs available via http://localhost:3000/docs
nerdctl run -it --rm -p 3000:80 note-api:latest

```