from typing import List

from fastapi import FastAPI, status, HTTPException, Depends
from database import Base, engine, SessionLocal
from sqlalchemy.orm import Session

import models
import schemas

# Create the database
Base.metadata.create_all(engine)

# Initialize app
app = FastAPI()

# Helper function to get database session
def get_session():
    session = SessionLocal()
    try:
        yield session
    finally:
        session.close()

@app.get("/")
def root():
    return "Post API"

@app.post("/post", response_model=schemas.Post, status_code=status.HTTP_201_CREATED)
def create_post(mypost: schemas.PostCreate, session: Session = Depends(get_session)):
    
    # create an instance of the ToDo database model
    post = models.Post(title = mypost.title, content = mypost.content)

    # add it to the session and commit it
    session.add(post)
    session.commit()
    session.refresh(post)

    # return the post object
    return post


@app.get("/post/{id}", response_model=schemas.Post)
def read_post(id: int, session: Session = Depends(get_session)):
    
    # get the todo item with the given id
    post = session.query(models.Post).get(id)

    # check if todo item with given id exists. If not, raise exception and return 404 not found response
    if not post:
        raise HTTPException(status_code=404, detail=f"[post with id {id} not found")

    return post

@app.put("/post/{id}", response_model=schemas.Post)
def update_post(id: int, mypost: schemas.PostCreate, session: Session = Depends(get_session)):
    
    # get the todo item with the given id
    post = session.query(models.Post).get(id)

    # update todo item with the given task (if an item with the given id was found)
    if post:
        post.title = mypost.title
        post.content = mypost.content
        session.commit()

    # check if todo item with given id exists. If not, raise exception and return 404 not found response
    if not post:
        raise HTTPException(status_code=404, detail=f"todo item with id {id} not found")

    return post

@app.delete("/post/{id}", status_code=status.HTTP_204_NO_CONTENT)
def delete_post(id: int, session: Session = Depends(get_session)):
    
    # get the todo item with the given id
    post = session.query(models.Post).get(id)

    # if todo item with given id exists, delete it from the database. Otherwise raise 404 error
    if post:
        session.delete(post)
        session.commit()
    else:
        raise HTTPException(status_code=404, detail=f"todo item with id {id} not found")

    return None

@app.get("/post", response_model = List[schemas.Post])
def read_posts(session: Session = Depends(get_session)):
    
    # get all todo items
    posts = session.query(models.Post).all()

    return posts


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80, debug=True, threaded=True)
