from datetime import datetime
from pydantic import BaseModel

# Create PostRequest Base Model
class PostCreate(BaseModel):
    title: str
    content: str

class Post(BaseModel):
    id: int
    title: str
    content: str
    created: datetime

    class Config:
        orm_mode = True